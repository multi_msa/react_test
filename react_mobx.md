[React & Mobx 실습]

- 프로젝트 생성
- 필요한 패키지 설치
- Mobx 설정(set_mobx.md파일 참조)
- store, container, view 패키지 작성
- store : State Management를 위한 Store 구현(state로 관리할 데이터 선언 및 초기화 @observable, @computed, @action ...)
- container : (@objserver, @inject(store명), store를 사용해서 view render, Event구현 view props전달 )
- view : (constainer로부터 props 받아 화면 display, evnethandler는 props로부터 전달받은 함수형객체 사용)
- App.js : (@observer) main page로 layout 작성해서 view composite하기 위해 container 배치
- index.js : Provider 이용 store 설정

[MobX 설정]

- yarn eject

- yarn add @babel/plugin-proposal-class-properties @babel/plugin-proposal-decorators

- Babel 설정

  ```json
    "babel": {
      "presets": [
        "react-app"
      ],
      "plugins": [
          ["@babel/plugin-proposal-decorators", { "legacy": true}],
          ["@babel/plugin-proposal-class-properties", { "loose": true}]
      ]
    }
  ```
