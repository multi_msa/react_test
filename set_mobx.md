[React 실습을 위한 기본 설정]

- node.js 설치
- npm을 통한 yarn 설치
- npm 혹은 yarn을 이용해 create-react-app 설치
- create-react-app 를 이용해 React 프로젝트 생성



[MobX 설정]

- yarn eject

- yarn add @babel/plugin-proposal-class-properties @babel/plugin-proposal-decorators

- Babel 설정

  ```json
    "babel": {
      "presets": [
        "react-app"
      ],
      "plugins": [
          ["@babel/plugin-proposal-decorators", { "legacy": true}],
          ["@babel/plugin-proposal-class-properties", { "loose": true}]
      ]
    }
  ```

  